class CreateMyModels < ActiveRecord::Migration
  def change
    create_table :my_models do |t|
      t.string :name
      t.string :tipousuario
      t.integer :reputacao
      t.integer :rank
      t.integer :level
      t.string :patente

      t.timestamps null: false
    end
  end
end
